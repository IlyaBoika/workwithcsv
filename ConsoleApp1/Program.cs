﻿using CsvHelper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using WorkWithCSV.CsvMapper;
using WorkWithCSV.Domain;
using WorkWithCSV.EntityFramework.Context;
using WorkWithCSV.Repositories;
using WorkWithCSV.Services;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new CategoryDbContext(new DbContextOptionsBuilder()
                         .UseSqlServer("Data Source=.\\SQLEXPRESS;Initial Catalog=CategoryDb;Integrated Security=True").Options);
            var categoryRepository = new EFCategoryRepository(context);
            // categoryRepository.Add(new Category {  Name = "OK", ParentId = 1 });
            //categoryRepository.Remove(1);
            var choosen = categoryRepository.GetById(10);
            Console.WriteLine(choosen.Name);
            Console.ReadLine();
            // string path = @"D:\categories.csv";

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"../../../../Resources\categories.csv");



            //using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            //{
            //    sr.ReadLine();
            //    string line;

            //    while ((line = sr.ReadLine()) != null)
            //    {
            //        var data = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            //        var category = new Category()
            //        { Id = Convert.ToInt32(data[0]), Name = data[1], ParentId = data[2] == "NULL" ? null : (int?)Convert.ToInt32(data[2]) };
            //        categoryRepository.Add(category);
            //    }
            //}


            //var repository = new EFCategoryRepository(context);
            //var deserializer = new Deserializer();
            //var records = deserializer.Deserialize(path);
            //var importProcessor = new ImportProcessor(repository);
            //importProcessor.Import(records);

        }
    }
}