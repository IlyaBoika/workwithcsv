﻿using CsvHelper.Configuration;
using WorkWithCSV.Domain;

namespace WorkWithCSV.CsvMapper
{
    public sealed class CategoryCsvMap : ClassMap<Category>
    {
        public CategoryCsvMap()
        {
            Map(m => m.Id).Name("Id");
            Map(m => m.Name).Name("Name");
            Map(m => m.ParentId).Name("ParentId").TypeConverterOption.NullValues("NULL");
        }
    }
}
