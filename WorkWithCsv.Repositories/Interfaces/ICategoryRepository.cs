﻿using System.Collections.Generic;
using WorkWithCSV.Domain;

namespace WorkWithCsv.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        void Add(Category category);
        List<Category> GetAllCategorys();
        Category GetById(int id);
        void Remove(int id);
        void Save(Category category);
    }
}
