﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using WorkWithCsv.Repositories.Interfaces;
using WorkWithCSV.Domain;
using WorkWithCSV.EntityFramework.Context;

namespace WorkWithCSV.Repositories
{
    public class EFCategoryRepository:ICategoryRepository
    {
        private readonly CategoryDbContext _context;

        public EFCategoryRepository(CategoryDbContext context)
        {
            _context = context;
        }

        public void Add(Category category)
        {
            _context.Add(category);
            _context.SaveChanges();
        }

        public List<Category> GetAllCategorys()
        {
            return _context.Categories.ToList();
        }

        public Category GetById(int id)
        {
            return _context.Categories.First(_ => _.Id == id);
        }

        public void Remove(int id)
        {
            Category category = new Category { Id = id };
            _context.Entry(category).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public void Save(Category category)
        {
            if (category.Id == 0)
            {
                _context.Add(category);
            }
            else
            {
                _context.Attach(category);
                _context.Entry(category).State = EntityState.Modified;
            }
            _context.SaveChanges();
        }


    }
}
