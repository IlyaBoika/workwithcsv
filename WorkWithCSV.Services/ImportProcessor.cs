﻿using System.Collections.Generic;
using System.Linq;
using WorkWithCSV.Domain;
using WorkWithCSV.Repositories;

namespace WorkWithCSV.Services
{
    public class ImportProcessor
    {
        private readonly EFCategoryRepository _categoryRepository;

        public ImportProcessor(EFCategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public void Import(IEnumerable<Category> records)
        {
            records.ToList();

            foreach (Category ithem in records)
            {
                _categoryRepository.Add(ithem);
            }
        }
    }
}
