﻿using System.Collections.Generic;
using WorkWithCsv.Repositories.Interfaces;
using WorkWithCSV.Domain;
using WorkWithCSV.Services.Interfaces;

namespace WorkWithCSV.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public void Add(Category category)
        {
            _categoryRepository.Add(category);
        }

        public List<Category> GetAllCategorys()
        {
            return _categoryRepository.GetAllCategorys();
        }

        public Category GetById(int id)
        {
            return _categoryRepository.GetById(id);
        }

        public void Remove(int id)
        {
            _categoryRepository.Remove(id);
        }

        public void Save(Category category)
        {
            _categoryRepository.Save(category);
        }
    }
}
