﻿using CsvHelper;
using System.Collections.Generic;
using System.IO;
using WorkWithCSV.CsvMapper;
using WorkWithCSV.Domain;

namespace WorkWithCSV.Services
{
    public class Deserializer
    {
        public IEnumerable<Category> Deserialize(string path)
        {
            var records = new List<Category>();

            using (TextReader fileReader = File.OpenText(path))
            {
                var csv = new CsvReader(fileReader);

                csv.Configuration.RegisterClassMap<CategoryCsvMap>();
                csv.Configuration.TypeConverterCache.AddConverter<int?>(new CustomInt32Converter());

                while (csv.Read())
                {
                    var record = csv.GetRecord<Category>();
                    records.Add(record);
                }
            }
            return records;
        }
    }
}
