﻿using System.Collections.Generic;
using WorkWithCSV.Domain;

namespace WorkWithCSV.Services.Interfaces
{
    public interface ICategoryService
    {
        void Add(Category category);
        List<Category> GetAllCategorys();
        Category GetById(int id);
        void Remove(int id);
        void Save(Category category);
    }
}
