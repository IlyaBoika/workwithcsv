﻿using CsvHelper;
using CsvHelper.TypeConversion;
using CsvHelper.Configuration;

namespace WorkWithCSV.CsvMapper
{
    public class CustomInt32Converter : Int32Converter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            if (text == "NULL") return null;
            return base.ConvertFromString(text, row, memberMapData);
        }
    }
}
