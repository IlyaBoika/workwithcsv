﻿using Microsoft.EntityFrameworkCore;
using WorkWithCSV.Domain;
using WorkWithCSV.EntityFramework.Mapping;

namespace WorkWithCSV.EntityFramework.Context
{
    public class CategoryDbContext : DbContext
    {
        public CategoryDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryMap());
        }
    }
}

