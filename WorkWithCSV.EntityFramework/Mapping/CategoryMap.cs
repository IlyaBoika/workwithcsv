﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WorkWithCSV.Domain;

namespace WorkWithCSV.EntityFramework.Mapping
{
    public class CategoryMap : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Category");

            builder.HasKey(_ => _.Id);

            builder.Property(_ => _.Id).HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(_ => _.Name).HasColumnName("Name").HasMaxLength(100).IsRequired();
            builder.Property(_ => _.ParentId).HasColumnName("ParentId");
        }
    }
}