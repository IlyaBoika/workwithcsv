﻿using System.Collections.Generic;
using WorkWithCSV.Domain;

namespace WorkWithCSV.WebApp.Models.Home
{
    public class HomeModel
    {
        public List<Category> Categories { get; set; }
    }
}
