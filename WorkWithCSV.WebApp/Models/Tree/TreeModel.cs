﻿using System.Collections.Generic;
using WorkWithCSV.Domain;

namespace WorkWithCSV.WebApp.Models.Tree
{
    public class SeededCategories
    {
        public int? Seed { get; set; }
        public List<Category> Categories { get; set; }
    }
}
