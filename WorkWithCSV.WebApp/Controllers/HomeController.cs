﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WorkWithCSV.Services.Interfaces;
using WorkWithCSV.WebApp.Models;

using WorkWithCSV.WebApp.Models.Home;
using WorkWithCSV.WebApp.Models.Tree;

namespace WorkWithCSV.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryService _categoryService;

        public HomeController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            var categories = _categoryService.GetAllCategorys();

            var model = new HomeModel
            {
                Categories = categories
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Remove(int id)
        {
            _categoryService.Remove(id);
            return RedirectToAction("About");
        }

        public IActionResult About()
        {
            var categories = _categoryService.GetAllCategorys();
            var model = new SeededCategories
            {
                Seed = null,
                Categories = categories
            };
            return View(model);
        }

        public ActionResult _TreeCategories()
        {
            var categories = _categoryService.GetAllCategorys();
            var model = new SeededCategories
            {
                Seed = null,
                Categories = categories
            };
            return PartialView(model);
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
