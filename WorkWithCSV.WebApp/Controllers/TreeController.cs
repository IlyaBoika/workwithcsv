﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using WorkWithCSV.Services.Interfaces;
using WorkWithCSV.WebApp.Models.Tree;

namespace WorkWithCSV.WebApp.Controllers
{
    public class TreeController : Controller
    {
        private readonly ICategoryService _categoryService;

        public TreeController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public IActionResult Index()
        {
            var categories = _categoryService.GetAllCategorys();
            var model = new SeededCategories
            {
                Seed = null,
                Categories = categories
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Remove(int id)
        {
            _categoryService.Remove(id);
            return RedirectToAction("Index");
        }

        public ActionResult _TreeCategories()
        {
            var categories = _categoryService.GetAllCategorys();
            var model = new SeededCategories
            {
                Seed = null,
                Categories = categories
            };
            return PartialView(model);
        }

        //public ActionResult Edit(int Id)
        //{
        //    var parentNode = _categoryService.GetById(Id);
        //    var nodes = _categoryService.GetAllCategorys().Select(_ => _.ParentId == parentNode.Id);
        //}
    }
}