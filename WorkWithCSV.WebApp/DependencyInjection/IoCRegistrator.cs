﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WorkWithCsv.Repositories.Interfaces;
using WorkWithCSV.EntityFramework.Context;
using WorkWithCSV.Repositories;
using WorkWithCSV.Services;
using WorkWithCSV.Services.Interfaces;

namespace WorkWithCSV.WebApp.DependencyInjection
{
    public class IoCRegistrator
    {
        public static void Register(IServiceCollection services)
        {
            services.AddDbContext<CategoryDbContext>(options => options.UseSqlServer(Constants.ConnectionString));

            services.AddScoped<ICategoryRepository, EFCategoryRepository>();
            services.AddScoped<ICategoryService, CategoryService>();
        }
    }
}
